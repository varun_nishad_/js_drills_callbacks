const cbFun = require('../callback3.js')

async function test(cb, id){
  try{
    const response = await cb(id)
    console.log(response)
    console.log('Details returned')
  }
  catch(err){
    console.log(err)
  }
}

test(cbFun, 'qwsa221')
