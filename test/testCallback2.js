const cbFun = require('../callback2.js')

async function test(cb, id){
  try{
    const response = await cb(id)
    console.log(response)
    console.log('Details Returned')
  }
  catch(err){
    console.log(err)
  }
}

test(cbFun, 'mcu453ed')
test(cbFun, '21dc83we')

