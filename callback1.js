
function boardsDetails(bid){
  let boardsData = require('./data/boards.json')

  return new Promise((resolve, reject)=>{
      setTimeout(() => {

        for(let obj of boardsData){
          if(obj['id'] == bid){
            resolve(obj)
          }
        }
        reject('Details not found')

      }, 2 * 1000);
    })
  }

  module.exports = boardsDetails
