function listDetails(bid){
    const listData = require('./data/lists.json')

    return new Promise((resolve, reject)=> {

      setTimeout(() => {

        for(let obj in listData){
          if(obj === bid){
            resolve(listData[obj])
          }
        }

        reject('Details not found')
      }, 2 * 1000);
    });

}

module.exports = listDetails

