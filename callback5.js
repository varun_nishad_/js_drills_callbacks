function getThanos(bid){
  const cb1 = require('./callback1.js')
  const cb2 = require('./callback2.js')
  const cb3 = require('./callback3.js')

  return new Promise((resolve,reject)=>{
    setTimeout(async ()=>{
      const Id = await cb1(bid);
      console.log(Id)
      const listId = await cb2(Id["id"]);
      console.log(listId)
      let mindId;
      let spaceId;
      for(const obj of listId){
        if(obj["name"] === "Mind"){
          mindId = obj["id"];
        }
        if(obj["name"] === "Space"){
          spaceId = obj["id"];
        }
      }
      resolve(Promise.all([cb3(mindId),cb3(spaceId)]))
    }
    ,2000)
  })
}

module.exports = getThanos

// getThanos('thanos')
