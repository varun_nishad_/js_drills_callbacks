function getThanos(bid){
  const cb1 = require('./callback1.js')
  const cb2 = require('./callback2.js')
  const cb3 = require('./callback3.js')

  return new Promise((resolve,reject)=>{
    setTimeout(async ()=>{
      const thanosId = await cb1(bid);
      console.log(thanosId)
      const listId = await cb2(thanosId["id"]);
      console.log(listId)
      let mindId;
      for(const obj of listId){
        if(obj["name"] === "Mind"){
          mindId = obj["id"];
        }
      }
      // const cards = await cb3(mindID)
      resolve(await cb3(mindId));
    }
    ,2000)
  })
}

module.exports = getThanos

// getThanos('thanos')
