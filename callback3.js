function cardDetails(lid) {
  const cardsData = require('./data/cards.json');

  return new Promise((resolve, reject) => {

    setTimeout(() => {
      for (let obj in cardsData) {
        if (obj === lid) {
          resolve(cardsData[obj])
        }
      }
      reject('Details not found')

    }, 2 * 1000);
  })

}

module.exports = cardDetails
